. ${SH:=$MODPATH/ohmyfont}

### INSTALLATION ###

ui_print '- Installing'

ui_print '+ Prepare'
prep

ui_print '+ Configure'
config

ui_print '+ Google Sans'
[ "${ROUNDED:=`valof ROUNDED`}" = true ] && {
    ui_print '  Rounded'
    mv $FONTS/GoogleSansRounded-$Re$X $FONTS/$SS
}
install_font

src

ui_print '+ Rom'
[ $PXL ] && {
    [ "${GSOPSZ:=`valof GSOPSZ`}" = false ] || \
    for i in $FW; do i=`up $i`
        eval U$i=\"`eval echo $"U$i" | sed 's|opsz[ [:digit:]]*||'`\"
    done
    axis_del=false
}
rom

ui_print '- Finalizing'
fontspoof

# chrome shows italic on xda
rm $PRDFONT/$GSR

svc
finish
